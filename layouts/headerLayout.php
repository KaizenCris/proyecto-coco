	<div id="header" >
		
		<nav class="navbar navbar-default" role="navigation">
		  <!-- Brand and toggle get grouped for better mobile display -->
		  <div class="navbar-header">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		      <span class="sr-only">Toggle navigation</span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		    </button>
		    <a class="navbar-brand" href="#">
		    	<img src="<?php echo $layoutsDirectory; ?>/img/logoCeti.gif" class="img-rounded img-responsive col-md-2"  alt="Responsive image">
		    </a>
		  </div>

		  <!-- Collect the nav links, forms, and other content for toggling -->
		  <div class="collapse navbar-collapse navbar-ex1-collapse">
		  	<ul class="nav navbar-nav navbar-left">
		  	</ul>
			<?php	if((isset($_SESSION['logged']))!=1){  ?>
		    <form class="navbar-form navbar-right" action="<?php echo $rootDirectory; ?>/controller/userController/login.php" method="post">
		      <div class="form-group col-lg-5">
			    <label class="sr-only" for="Username">Correo Electronico</label>
			    <input type="email" class="form-control" id="Username" name="Username" placeholder="Correo Electronico">
			  </div>
			  <div class="form-group col-lg-5">
			    <label class="sr-only" for="Password">Password</label>
			    <input type="password" class="form-control" id="Password" name="Password" placeholder="Password">
			  </div>
			  <button type="submit" class="btn btn-warning col-lg-2">Log in</button>
		    </form>
		    <?php	}else{  ?>
			<h1 style = "float: right; margin-right:30px;"><a href="<?php echo $rootDirectory; ?>/controller/userController/logout.php">LOG OUT</a></h1>
			
			<?php 	} ?>
		  </div><!-- /.navbar-collapse -->
		</nav>
		<!-- el login es cargado por Php desde controlador dependiendo del estatus de session,

		Vista debe de diseñar esto procurando diseñar un codigo lo mas limpio posible-->
		
	</div>