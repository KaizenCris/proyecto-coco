<nav class="navbar navbar-default " role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse navbar-center">
    <a class="navbar-brand" href="#">CETI Proyectos</a>
    <ul class="nav navbar-nav">
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Carreras <b class="caret"></b></a>
        <ul class="dropdown-menu">
           <li><a href="addCareer.php">Agregar Carrera</a></li>
           <li><a href="modifieCareer.php">Modificar Carrera</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Categorias <b class="caret"></b></a>
        <ul class="dropdown-menu">
           <li><a href="addCategory.php">Agregar Categoría</a></li>
           <li><a href="modifieCategory.php">Modificar Categoría</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Generación <b class="caret"></b></a>
        <ul class="dropdown-menu">
           <li><a href="addGeneration.php">Agregar Generación</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Usuarios <b class="caret"></b></a>
        <ul class="dropdown-menu">
           <li><a href="addUser.php">Agregar Usuario</a></li>
           <li><a href="modifieUser.php">Modificar Perfil de usuario</a></li>
        </ul>
      </li>
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Proyectos <b class="caret"></b></a>
        <ul class="dropdown-menu">
           <li><a href="allProjects.php">Ver Proyectos</a></li>
           <li><a href="modifieProject.php">Modificar Proyecto</a></li>
           <li><a href="deleteProject.php">Eliminar Proyecto</a></li>
        </ul>
      </li>
     
      
      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#">Log Out</a></li>
    </ul>
    
  </div><!-- /.navbar-collapse -->
</nav>



<script type="text/javascript"src="js/jquery.js"> </script>
<script type="text/javascript"src="js/bootstrap.js"> </script>