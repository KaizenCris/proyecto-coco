<?php

	$rootDirectory = '../..';
	include(rootDirectory.'/model/Users.php');
	include(rootDirectory.'/model/Generations.php');
	include(rootDirectory.'/model/Careers.php');

	if(!isset(($_SESSION['logged']))){//un usuario no logueado no puede ver a los usuarios
		header($rootDirectory."/view/index.php");
	}else{
		if($_SESSION['userType']==2){//un alumno no puede ver a todos los usarios, unicamente al suyo
			header($rootDirectory."/view/index.php");
		}
	}
	//Llenar todos los datos

	$user= new User;
	$generation= new Generation;
	$career = new Career;
	
	
	$data=$user->all();
	$i =0;
	foreach($file in $data){
		$gen = $generation->single($file->idgeneration);
		$career = $career->single($file->idcareer);
		$user[$i] = array('idUser'=>$file->id, 	'name'=>$file->name, 	'type'=>$data->type	);
		$generacion[$i]	= array('nombreGen'=>$gen);
		$carrera[$i] = array('nombreCarrera'=>$career);
	}
	$controllerData = array(
		'user'		=> $user,
		'generacion'	=> $generacion,
		'carrera'		=> $carrera
     );
?>var controllerData = <?php echo json_encode($controllerData); ?>;