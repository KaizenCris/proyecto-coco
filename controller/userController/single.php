<?php

	$rootDirectory = '../..';
	include(rootDirectory.'/model/Users.php');
	include(rootDirectory.'/model/Generations.php');
	include(rootDirectory.'/model/Careers.php');

	if(!isset(($_SESSION['logged']))){//un usuario no logueado no puede ver a los usuarios
		header($rootDirectory."/view/index.php");
	}
	//Llenar todos los datos

	$user = new User;
	$generation= new Generation;
	$career = new Career;
	$id = @($_POST["id"]);
	
	$data=$user->single($id);
	$gen = $generation->single($data->idgeneration);
	$career = $career->single($data->idcareer);
	$controllerData=array(
		'user'=> array(array('idUser'=>$id, 	'name'=>$data->name, 	'type'=>$data->type		)),
		  
		  'generacion'	=> array(array('nombreGen'=>$gen),

		  'carrera'		=> array(array('nombreCarrera'=>$career))
     );
?>var controllerData = <?php echo json_encode($controllerData); ?>;