<?php
	/***************************************************************
	Su tarea equipo controlador es mantener control en las variables de session para el acceso de las paginas. 
	Quien puede entrar quien no. Dependiendo de esto, de la informacion que ustedes obtienen, que pueden ver y que no.
	Tienen que ir obtener informacion de la base de datos segun el modelo lo permita. 
	Filtrar y parcear esta informacion para devolverla por este medio "controllerData"
	Restringir el acceso a directorios no permitidos al usuario. (coco o hutch les pueden decir muy bien como hacer esto. Lo logran con httpd.conf de Apache.)
	Eso es todo el trabajo.
	
	*En caso de no tener derecho a entrar en una pagina redireccionenlos.
	*Su otra labor es inpedir el accedo a directorios que no tiene permiso el usuario
	*En otras palabras, no le permitan acceder directamente a las carpetas del sistema. usen Apache redireccionarlos.
	*Cambien los nombres de los directorios: 
	quiten el view de http://localhost/proyectosCETI/view/ por medio de apache.
	coco conoce bien acerca de este tema. Documentense con el, les sera de utilidad
	****************************************************************/
	//este array sera parceado a JSON para mandarlo a vista y que ellos interpreten los datos
	$controllerData = array(
          'logged'    	=> 'false', 			// true, false
		  'userName'	=> 'MrDonLic',
		  'name'		=> 'Eduardo de Jesus Aguilar Placencia',

		  
		  
		  'user'	=> array(
						  array('nombreUsuario'=>'nombre 1'),
						  array('nombreUsuario'=>'nombre 2'),
						  array('nombreUsuario'=>'nombre 3')
						),
		  
		  
		  'generacion'	=> array(
						  array('nombreGen'=>'2009'),
						  array('nombreGen'=>'2010'),
						  array('nombreGen'=>'2011')
						),

		  'proyecto'		=> array(
						  array('nombreProyecto'=>'Proyecto 1'),
						  array('nombreProyecto'=>'Proyecto 2'),
						  array('nombreProyecto'=>'Proyecto 3')
						)
     );
?>	var controllerData = <?php echo json_encode($controllerData); ?>;