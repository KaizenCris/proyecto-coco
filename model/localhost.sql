-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 29-10-2013 a las 14:35:48
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `proyectococo`
--
CREATE DATABASE `proyectococo` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `proyectococo`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrer`
--

CREATE TABLE IF NOT EXISTS `carrer` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `carrer`
--

INSERT INTO `carrer` (`Id`, `Name`) VALUES
(1, 'Informatica'),
(2, 'Electronica'),
(3, 'Electrotecnia'),
(4, 'Plasticos'),
(5, 'Construccion Buuuu');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `category`
--

INSERT INTO `category` (`Id`, `Name`) VALUES
(1, 'Videojuegos'),
(2, 'Programacion Web'),
(3, 'Aplicaciones Moviles'),
(4, 'Multimedia'),
(5, 'Electronica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `generation`
--

CREATE TABLE IF NOT EXISTS `generation` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `generation`
--

INSERT INTO `generation` (`Id`, `Name`) VALUES
(1, '2008-2012A'),
(2, '2008-2012B'),
(3, '2009-2013A'),
(4, '2009-2013B'),
(5, '2010-2014A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(45) NOT NULL,
  `Description` varchar(200) NOT NULL,
  `DescriptionImg` varchar(100) NOT NULL,
  `Portada` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `project`
--

INSERT INTO `project` (`Id`, `Title`, `Description`, `DescriptionImg`, `Portada`) VALUES
(1, 'Grammer', 'Genera paginas web por medio de Grammer. Usa nuestra tecnologia drag&drop', 'Logo.png', 'DiseñoImagenGrammer.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(45) NOT NULL,
  `Password` varchar(45) NOT NULL,
  `Type` varchar(11) NOT NULL,
  `Name` varchar(45) NOT NULL,
  `Lastname` varchar(45) NOT NULL,
  `IdGeneration` int(11) NOT NULL,
  `IdProyect` int(11) NOT NULL,
  `IdCarrer` int(11) NOT NULL,
  `AvatarImg` varchar(100) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`Id`, `Username`, `Password`, `Type`, `Name`, `Lastname`, `IdGeneration`, `IdProyect`, `IdCarrer`, `AvatarImg`) VALUES
(1, 'kaizen', 'chowrede', 'Admin', 'Cristian Alejandro', 'Hernández Muñoz', 2, 1, 1, ''),
(2, 'lic', 'chowrede', 'User', 'Eduardo de Jesus', 'Aguilar Plascencia', 3, 1, 1, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
