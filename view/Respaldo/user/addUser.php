<?php
/***ESPECIFICAR $rootDirectory PARA CADA ARCHIVO DE CADA CARPETA PARA QUE FUNCIONE DE MANERA CORRECTA*******/
$rootDirectory = "../..";
$layoutsDirectory = $rootDirectory . "/layouts" ;
?>

<script type="text/javascript">
/*********************CONTROLADOR*********************************/
	<?php include_once($rootDirectory."/controller/projectController/add.php");?>	
</script>
<!-- ESTA ZONA ES DEL EQUIPO VISTA -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title> CETI Proyectos</title>
		<script lnguage="javascript">
            function abrirCerrar(div,div1)
            {
                
                var a = document.getElementById(div1);
                        $('#'+div1).slideDown('slow');
                        a.style.display = "block";
                    
                        $('#'+div).slideUp('slow');
                    
            }
        </script>
        <script language="javascript" src="js/jquery.js"></script>
		<link rel="shortcut icon" href="img/favicon.ico" />
	<!--Agrego css desde carpeta Layout-->
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/headerStyle.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/footerStyle.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<!--Agregen mas links Css y Javascript AQUI-->
		<script src="js/addProjectOnload.js"></script>
	</head>
	<body>

	
		<div class="container">
			
			<?php include_once($layoutsDirectory."/adminLayout.php"); ?>
			<div class="jumbotron">
									<div class="row">
						<div class="progress progress-striped active">
							<div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
						</div>
					</div>
					<div class="row">
						<div class="page-header text-center">
						 	<h1>Agregar <small>Usuario</small></h1>
						</div>
					</div>
					<div class="row">
						<div class="progress progress-striped active">
							<div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
						</div>
					</div>
					<div class="row" id="a">
					<!-- en el formularioa cada campo se llama como en la base de datos solo falta el action=""
					-->
						<div class="page-header text-center">
						 	<h2>Datos del <small>Usuario</small></h2>
						</div>
						<form class="form-horizontal" role="form" action="../../controller/userController/add.php" method="post">
							<div>
							<div class="col-lg-12 form-group">
							  	<spam  class="col-lg-offset-2 col-lg-2 label label-warning text-center">User Name</spam>
							    <div class="col-lg-6">
							    	<input type="text" name="Username" class="form-control text-center"  placeholder="Nombre de Usuario">
							    	<span class="help-block"><h5>En este campo tu pondras el Nombre con el que el Usuario iniciara Secion</h5></span>
							    </div>
							</div>
							<div class="col-lg-12 form-group">
							  	<spam  class="col-lg-offset-2 col-lg-2 label label-warning text-center">Password</spam>
							    <div class="col-lg-6">
							    	<input type="password" name="Password" class="form-control text-center"  placeholder="Password">
							    	<span class="help-block"><h5>En este campo tu pondras la Contraseña con la que el Usuario iniciara Secion</h5></span>
							    </div>
							</div>
							<div class="col-lg-12 form-group">
							<spam  class="col-lg-offset-2 col-lg-2 label label-warning text-center">Prioridad</spam>
							<div class="col-md-6">
								<select class="form-control" name="Type">
									<option>Super Usuario</option>
									<option>Administrador</option>
								</select>
								<span class="help-block"><h5>En este campo tu pondras el tipo de Usuraio para darle los permisos correspondientes</h5></span>
							</div>
							</div>
							<div class="col-lg-12 form-group">
							</div>
							</div>
							<div class="col-lg-12 form-group">
							    <div class="col-lg-offset-8 col-lg-2">
							      <a href="javascript:void(0);" onclick="abrirCerrar('a','b')"><span class="label label-warning">Siguiente</button></a>
							    </div>
							</div>
						

					</div>
					<div class="row" id="b" style=" display: none;" >
					<!-- en el formularioa cada campo se llama como en la base de datos solo falta el action=""
					-->
						<div class="page-header text-center">
						 	<h2>Datos de <small>Usuario</small></h2>
						</div>
						
							<div>
							  	<div class="col-lg-12 form-group">
							  	<spam  class="col-lg-offset-2 col-lg-2 label label-warning text-center">Nombre</spam>
							    <div class="col-lg-6">
							    	<input type="text" name="Name" class="form-control text-center"  placeholder="Nombre">
							    	<span class="help-block"><h5>En este campo tu pondras el Nombre del Usuario</h5></span>
							    </div>
								</div>
								<div class="col-lg-12 form-group">
							  	<spam  class="col-lg-offset-2 col-lg-2 label label-warning text-center">Apellido</spam>
							    <div class="col-lg-6">
							    	<input type="text" name="Lastname" class="form-control text-center"  placeholder="Apellido">
							    	<span class="help-block"><h5>En este campo tu pondras el Apellido del Usuario</h5></span>
							    </div>
								</div>
								<div class="col-lg-12 form-group">
							  	<spam  class="col-lg-offset-2 col-lg-2 label label-warning text-center">Avatar</spam>
							    <div class="col-lg-6">
							    	<input type="file" accept="image/*" name="AvatarImg" class="form-control text-center"  >
							    	<span class="help-block"><h5>En este campo tu pondras Imagen de Avatar del Usuario</h5></span>
							    </div>
								</div>
							</div>
							<div class="col-lg-12 form-group">
							    <div class="col-lg-offset-8 col-lg-2">
							      <a href="javascript:void(0);" onclick="abrirCerrar('b','c')"><span class="label label-warning">Siguiente</button></a>
							    </div>
							</div>
						

					</div>

					<div class="row" id="c" style=" display: none;">
					<!-- en el formularioa cada campo se llama como en la base de datos solo falta el action=""
					-->
					<div class="page-header text-center">
						 	<h2>Datos del <small>Proyecto</small></h2>
						</div>
						
							<div>
							  <div class="col-lg-12 form-group">
							<spam  class="col-lg-offset-2 col-lg-2 label label-warning text-center">Generacion</spam>
							<div class="col-md-6">
								<select class="form-control" name="IdGeneration">
									<option>Generacion</option>
								</select>
								<span class="help-block"><h5>En este campo tu pondras Selecionar la Generacion del Usuario</h5></span>
							</div>
							</div>
							<div class="col-lg-12 form-group">
							<spam  class="col-lg-offset-2 col-lg-2 label label-warning text-center">Proyecto</spam>
							<div class="col-md-6">
								<select class="form-control" name="IdProyect">
									<option>Proyecto</option>
								</select>
								<span class="help-block"><h5>En este campo tu pondras Selecionar El Proyecto</h5></span>
							</div>
							</div>
							<div class="col-lg-12 form-group">
							<spam  class="col-lg-offset-2 col-lg-2 label label-warning text-center">Carrera</spam>
							<div class="col-md-6">
								<select class="form-control" name="IdCarrer">
									<option>Carrera</option>
								</select>
								<span class="help-block"><h5>En este campo tu pondras Selecionar la Carrera de la cual es el Usuario</h5></span>
							</div>
							</div>	
							</div>
							<div class="col-lg-12 form-group">
							    <div class="col-lg-offset-8 col-lg-2">
							      <button type="submit" class="btn btn-warning">cargar</button>
							    </div>
							</div>
						</div>
						</form>
						
					
					<div class="row">
						<div class="progress progress-striped active">
							<div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
						</div>
					</div>
			</div>
			

			<?php include_once($layoutsDirectory."/footerLayout.php"); ?>
		</div>


	</body>
</html>