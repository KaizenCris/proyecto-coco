//**********ONLOAD FUNCTION**********************************//
window.onload=function(){			
	//DEBUG
	var debug = document.createElement("div");
	debug.setAttribute("id","debug"); 
	debug.setAttribute("class","correct"); 
	footer = document.getElementById("footer");
	footer.appendChild(debug);
	
	
	//AGREGAR EVENTOS
		//Ejemplos:   	todos tus eventos se agregan aqui. Si tienes duda en el modo de uso revisar eventos aqui http://www.w3schools.com/jsref/dom_obj_event.asp 
		//temp = document.getElementById("interfazDeEdicion");
		//temp.addEventListener('dragenter', handleDragEnter, false);
		//temp.addEventListener('dragover', handleDragOver, false);
		//temp.addEventListener('dragleave', handleDragLeave, false);
		//temp.addEventListener('drop', handleDrop, false);
		//temp.addEventListener('dragend', handleDragEnd, false);
	debug.innerHTML = "Mensaje: Single project Onload";
	};

/***************EJEMPLO DEFUNCIONES****************************
var dragSrcEl = null;
var dragOrigin = 0;
function handleDragStart(e) {
  this.style.opacity = '0.4';  // this / e.target is the source node.
  dragSrcEl = this;

  e.dataTransfer.effectAllowed = 'copy';
  e.dataTransfer.setData('text/html', this.innerHTML);
}

function handleDragOver(e) {
	if (e.preventDefault) {
		e.preventDefault(); // Necessary. Allows us to drop.
	}
	if(dragSrcEl!=null){
		  
		e.dataTransfer.dropEffect = 'copy';  // See the section on the DataTransfer object.
		this.classList.add('over');
		this.parentNode.classList.remove('over');
	} 
	return false;
} 
****************************************************************/