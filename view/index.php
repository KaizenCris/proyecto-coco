<?php
session_Start();
/***ESPECIFICAR $rootDirectory PARA CADA ARCHIVO DE CADA CARPETA PARA QUE FUNCIONE DE MANERA CORRECTA*******/
$rootDirectory = "..";
$layoutsDirectory = $rootDirectory . "/layouts" ;
?>
<script type="text/javascript">
/*********************CONTROLADOR*********************************/
	<?php include_once($rootDirectory."/controller/indexController.php"); ?>
	
    /****** Ejemplo de como acceder a los datos desde vista********
	//Si tienes duda de los elementos de controller data visita  "/controller/indexController.php"
	alert(controllerData['logged']);
    alert(controllerData['name']);
    alert(controllerData['newestProjects'][0]['image']);
	alert(controllerData['newestProjects'][0]['name']);
	alert(controllerData['newestProjects'][0]['description']);
	/**************************************************************/
</script>
<!-- ESTA ZONA ES DEL EQUIPO VISTA -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title> CETI Proyectos</title>
	<!--Agrego css desde carpeta Layout-->
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/headerStyle.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/footerStyle.css" media="screen" />
	
	<!--Agregen mas links Css y Javascript AQUI-->
		<script src="js/indexOnload.js"></script>

		<meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    
	    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/estilo.css">

	</head>
	<body>
	
	<div id="content">
	
		<div class="container">
			<?php include_once($layoutsDirectory."/headerLayout.php"); ?>
			
			<div class="jumbotron">
				<div class="row top">
				<div id="myCarousel" class="carousel slide">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				    <li data-target="#myCarousel" data-slide-to="1"></li>
				    <li data-target="#myCarousel" data-slide-to="2"></li>
				  </ol>

				  <!-- Wrapper for slides -->
				  <div class="carousel-inner">

				  	<?php 
				  	$items=$controllerData['newestProjects'];
				  	$primero = true;

				  	foreach ($items as $item) 
				  	{
				  		if($primero == true)
				  		{
				  			$primero = false ?>

				  			<div class="item active">
						      <a href="#"><img src=<?php echo $item['image']; ?>  alt="Responsive image"></a>
						      <div class="container">
					            <div class="carousel-caption">
					              <h1><?php echo $item['description']; ?></h1>
					              <p><button class="btn btn-warning">Ver mas</button></p>
					            </div>
					          </div>
						    </div>
						    <?php
				  		}
				  		else
				  		{
				  			?>
				  			<div class="item">
						      <a href="#"><img src=<?php echo $item['image'] ?>  alt="Responsive image"></a>
						      <div class="container">
					            <div class="carousel-caption">
					              <h1><?php echo $item['description']; ?></h1>
					              <p><button class="btn btn-warning">Ver mas</button></p>
					            </div>
					          </div>
						    </div>
						    <?php

				  		}
				  	}

				  	$indice=0;

				  	 ?>				    
				    
				  </div>

				  <!-- Controls -->
				  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
				    <span class="icon-prev"></span>
				  </a>
				  <a class="right carousel-control" href="#myCarousel" data-slide="next">
				    <span class="icon-next"></span>
				  </a>

				</div>
			</div>
		
			<div class="row">
	            <div class="col-6 col-sm-6 col-lg-4">
	              <h2>Proyecto</h2>
	              <img src="img/proyecto1.jpg" class="img-rounded col-md-12">
	              <p class="top">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	              proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
	              <p>
	              <p><a class="btn btn-warning" href="#">Ver Detalles &raquo;</a></p>
	            </div><!--/span-->
	            <div class="col-6 col-sm-6 col-lg-4">
	              <h2>Proyecto</h2>
	              <img src="img/proyecto1.jpg" class="img-rounded col-md-12">
	              <p class="top">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	              proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
	              <p>
	              <a class="btn btn-warning" href="#">Ver Detalles &raquo;</a></p>
	            </div><!--/span-->
	            <div class="col-6 col-sm-6 col-lg-4">
	              <h2>Proyecto</h2>
	              <img src="img/proyecto1.jpg" class="img-rounded col-md-12">
	              <p class="top">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
	              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
	              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
	              proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
	              <p>
	              <p><a class="btn btn-warning" href="#">Ver Detalles &raquo;</a></p>
	            </div><!--/span-->
	          </div><!--/row-->


			</div>
			</div>


			

		<div class="bestProjects">
			<!-- revisar el documento a lado de index.php-->
		</div>

	</div>
		<?php include_once($layoutsDirectory."/footerLayout.php"); ?>

		<!-- Librerias JS-->
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>

	</body>
</html>