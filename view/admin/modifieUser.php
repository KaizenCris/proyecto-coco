<?php
/***ESPECIFICAR $rootDirectory PARA CADA ARCHIVO DE CADA CARPETA PARA QUE FUNCIONE DE MANERA CORRECTA*******/
$rootDirectory = "../..";
$layoutsDirectory = $rootDirectory . "/layouts" ;
?>

<script type="text/javascript">
/*********************CONTROLADOR*********************************/
	<?php include_once($rootDirectory."/controller/userController/modifie.php");?>	
</script>
<!-- ESTA ZONA ES DEL EQUIPO VISTA -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title> CETI Proyectos</title>
		<link rel="shortcut icon" href="img/favicon.ico" />
	<!--Agrego css desde carpeta Layout-->
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/headerStyle.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/footerStyle.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<!--Agregen mas links Css y Javascript AQUI-->
		<script src="js/addProjectOnload.js"></script>
	</head>
	<body>

	
		<div class="container">
			
			<?php include_once($layoutsDirectory."/adminLayout.php"); ?>
			<div class="jumbotron">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<div class="col-md-6">
							<select class="form-control" id="Type">
								<option>Id Username</option>
							</select>
						</div>
					</div>					
					<div class="form-group">
						
						<div class="col-md-6">
							<input type="text" class="form-control" placeholder="Nombre de Usuario" id="username">
						</div>
						<div class="col-md-6">
							<input type="password" class="form-control" placeholder="password" id="password" disabled>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4">
							<select class="form-control" id="Type">

								<?php 
									$typeUser=$controllerData['user'];
									foreach ($typeUser as $type) 
									{
										?>
										<option><?php echo $type['type']; ?></option>
										<?php
									}
								 ?>
								
							</select>
						</div>
						<div class="col-md-4">
							<input type="text" class="form-control" placeholder="Nombre" id="Name">
						</div>
						<div class="col-md-4">
							<input type="text" class="form-control" placeholder="Apellido" id="Lastname">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4">
							<select class="form-control" id="generacion">
								<?php 
								$nombGen = $controllerData['generacion'];
								foreach ($nombGen as $gen) 
								{
									?>
									<option><?php echo $gen['nombreGen'] ?></option>
									<?php 		
								} 	
								 ?>
							</select>
						</div>
						<div class="col-md-4">
							<select class="form-control" id="carrera">
								<?php 
								$nombCarre = $controllerData['carrera'];
								foreach ($nombCarre as $name) 
								{
									?>
									<option><?php echo $name['nombreCarrera'] ?></option>
									<?php 		
								} 	
							 	?>
							</select>
						</div>
						<div class="col-md-4">
							<input type="file" class="form-control" id="AvatarImg">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-3 col-md-offset-9">
							<button type="submit" class="btn btn-warning form-control">Guardar</button>
						</div>
					</div>
				</form>
			</div>
			

			<?php include_once($layoutsDirectory."/footerLayout.php"); ?>
		</div>


	</body>
</html>