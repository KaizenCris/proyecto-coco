<?php
/***ESPECIFICAR $rootDirectory PARA CADA ARCHIVO DE CADA CARPETA PARA QUE FUNCIONE DE MANERA CORRECTA*******/
$rootDirectory = "../..";
$layoutsDirectory = $rootDirectory . "/layouts" ;
?>

<script type="text/javascript">
/*********************CONTROLADOR*********************************/
	<?php include_once($rootDirectory."/controller/projectController/all.php");?>	
</script>
<!-- ESTA ZONA ES DEL EQUIPO VISTA -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title> CETI Proyectos</title>
		<link rel="shortcut icon" href="img/favicon.ico" />
	<!--Agrego css desde carpeta Layout-->
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/headerStyle.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/footerStyle.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<!--Agregen mas links Css y Javascript AQUI-->
		<script src="js/addProjectOnload.js"></script>
	</head>
	<body>
		
		<div id="content">
		
			<div class="container">
				<?php include_once($layoutsDirectory."/adminLayout.php"); ?>
				<div class="jumbotron">
					<form class="form-horizontal" role="form" method="get">
						<div class="form-group">
							<div class="col-lg-4">
								<select class="form-control" id="carrera">
									<?php 
										$nombCarre = $controllerData['carrera'];
										foreach ($nombCarre as $name) 
										{
											?>
											<option><?php echo $name['nombreCarrera'] ?></option>
											<?php 		
										} 	
								 	?>
								</select>
							</div>
							<div class="col-lg-4">
								<select class="form-control" id="generacion">
									<?php 
										$nombGen = $controllerData['generacion'];
										foreach ($nombGen as $gen) 
										{
											?>
											<option><?php echo $gen['nombreGen'] ?></option>
											<?php 		
										} 	
									 ?>
								</select>
							</div>
							<div class="col-lg-4">
								<select class="form-control" id="categoria">
									<?php 
										$nombCateg = $controllerData['categoria'];
										foreach ($nombCateg as $nameCateg) 
										{
											?>
											<option><?php echo $nameCateg['nombreCategoria'] ?></option>
											<?php 		
										} 	
									 ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="page-header">
								<h2>Titulo del Proyecto</h2>
							</div>
							
							<div class="col-lg-9">
								<img src="#" class="img-rounded" width="100%" height="40%">
							</div>
							<div class="col-lg-3">
								<input type="search" class="form-control" placeholder="Buscar">
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-9">
								<textarea class="form-control" rows="10" disabled placeholder="Descripción del Proyecto"></textarea>
							</div>
						</div>
					</form>
				</div>
				<?php include_once($layoutsDirectory."/footerLayout.php"); ?>

			</div>

			
			
		</div>
		
	</body>
</html>