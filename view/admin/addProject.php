<?php
/***ESPECIFICAR $rootDirectory PARA CADA ARCHIVO DE CADA CARPETA PARA QUE FUNCIONE DE MANERA CORRECTA*******/
$rootDirectory = "../..";
$layoutsDirectory = $rootDirectory . "/layouts" ;
?>

<script type="text/javascript">
/*********************CONTROLADOR*********************************/
	<?php include_once($rootDirectory."/controller/projectController/modifie.php");?>	
</script>
<!-- ESTA ZONA ES DEL EQUIPO VISTA -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title> CETI Proyectos
		</title>
		<link rel="shortcut icon" href="img/favicon.ico" />
	<!--Agrego css desde carpeta Layout-->
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/headerStyle.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/footerStyle.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<!--Agregen mas links Css y Javascript AQUI-->
		<script src="js/addProjectOnload.js"></script>
	</head>
	<body>
		<div id="content">
			<div class="container">
				<?php include_once($layoutsDirectory."/adminLayout.php"); ?>
				<div class="jumbotron">
										<div class="row">
						<div class="progress progress-striped active">
							<div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
						</div>
					</div>
					<div class="row">
						<div class="page-header text-center">
						 	<h1>Agregar <small>Proyecto</small></h1>
						</div>
					</div>
					<div class="row">
						<div class="progress progress-striped active">
							<div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
						</div>
					</div>
					<div class="row">
					<!-- en el formularioa cada campo se llama como en la base de datos solo falta el action=""
					-->
						<form class="form-horizontal" role="form" action="../../controller/projectController/add.php" method="post">
							<div class="col-lg-12 form-group">
							  	<spam  class="col-lg-offset-2 col-lg-2 label label-warning text-center">Titulo</spam>
							    <div class="col-lg-6">
							    	<input type="text" id="Title" class="form-control text-center"  placeholder="Titulo de tu Proyecto">
							    	<span class="help-block"><h5>En este campo tu pondras el Nombre con el que se identifica tu proyecto.</h5></span>
							    </div>
							</div>
							<div class="col-lg-12 form-group">
							    <div class="col-lg-offset-2 col-lg-6">
							    	<input type="file" accept="image/*" id="Front" class="form-control">
							    	<span class="help-block"><h5>En este campo tu pondras la Imagen con la que se presentara tu proyecto.</h5></span>
							    </div>
							  	<spam  class="col-lg-2 label label-warning text-center">Portada</spam>
							</div>
							<div class="col-lg-12 form-group">
							  	<spam  class="col-lg-offset-1 col-lg-2 label label-warning text-center">Descripcion</spam>
							    <div class="col-lg-5">
							    	<textarea rows="4" cols="50" type="text" id="Description" class="form-control text-center"  placeholder="Descripcion de tu Proyecto"></textarea>
							    	<span class="help-block"><h5>En este campo tu pondras la Descrocion de tu proyecto cuentas con 200 caracteres.</h5></span>
							    </div>
							    <div class="col-lg-4">
							    	<div class="row">
							    		<div class="col-md-12">
							    		<spam  class="label label-warning text-center">Imagen</spam>	
							    		</div>
							    	</div>
							    	<div class="row">
							    		<input type="file" accept="image/*" id="DescriptionImg" class="form-control">
							    		<span class="help-block"><h5>En este campo tu pondras la Imagen que apolle a tu Descripcion.</h5></span>
							    	</div>
							    </div>
							</div>
							<div class="col-lg-12 form-group">
							  	<spam  class="col-lg-offset-1 col-lg-2 label label-warning text-center">Planeacion</spam>
							    <div class="col-lg-5">
							    	<textarea rows="4" cols="50" type="text" id="Planning" class="form-control text-center"  placeholder="Descripcion de tu Proyecto"></textarea>
							    	<span class="help-block"><h5>En este campo tu pondras tu Planeacion de tu proyecto cuentas con 200 caracteres.</h5></span>
							    </div>
							    <div class="col-lg-4">
							    	<div class="row">
							    		<div class="col-md-12">
							    		<spam  class="label label-warning text-center">Imagen</spam>	
							    		</div>
							    	</div>
							    	<div class="row">
							    		<input type="file" accept="image/*" id="PlanningImg" class="form-control">
							    		<span class="help-block"><h5>En este campo tu pondras la Imagen de tu Planeacion.</h5></span>
							    	</div>
							    </div>
							</div>
<!--
Descripcion	area
imagen		file
planeacion	area
imagen		file
-->
							<div class="col-lg-12 form-group">
							    <div class="col-lg-offset-8 col-lg-2">
							      <button type="submit" class="btn btn-default">Levantar</button>
							    </div>
							</div>
						</form>
					</div>
					<div class="row">
						<div class="progress progress-striped active">
							<div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
						</div>
					</div>
				</div>



			</div>
		</div>
		<?php include_once($layoutsDirectory."/footerLayout.php"); ?>
	</body>
</html>