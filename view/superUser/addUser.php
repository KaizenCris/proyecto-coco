<?php
/***ESPECIFICAR $rootDirectory PARA CADA ARCHIVO DE CADA CARPETA PARA QUE FUNCIONE DE MANERA CORRECTA*******/
$rootDirectory = "../..";
$layoutsDirectory = $rootDirectory . "/layouts" ;
?>

<script type="text/javascript">
/*********************CONTROLADOR*********************************/
	<?php include_once($rootDirectory."/controller/projectController/modifie.php");?>	
</script>
<!-- ESTA ZONA ES DEL EQUIPO VISTA -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title> CETI Proyectos</title>
		<link rel="shortcut icon" href="img/favicon.ico" />
	<!--Agrego css desde carpeta Layout-->
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/headerStyle.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/footerStyle.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<!--Agregen mas links Css y Javascript AQUI-->
		<script src="js/addProjectOnload.js"></script>
	</head>
	<body>

	
		<div class="container">
			
			<?php include_once($layoutsDirectory."/superUserLayout.php"); ?>
			<div class="jumbotron">
									<div class="row">
						<div class="progress progress-striped active">
							<div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
						</div>
					</div>
					<div class="row">
						<div class="page-header text-center">
						 	<h1>Agregar <small>Usuario</small></h1>
						</div>
					</div>
					<div class="row">
						<div class="progress progress-striped active">
							<div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
						</div>
					</div>
					<form class="form-horizontal" role="form" action="../../controller/userController/add.php" method="post">				
					<div class="form-group">
						
						<div class="col-md-6">
							<input type="text" class="form-control" placeholder="Nombre de Usuario" id="username">
						<span class="help-block"><h5>En este campo tu pondras el Nombre con el que el Usuario iniciara Secion</h5></span>
						</div>
						<div class="col-md-6">
							<input type="password" class="form-control" placeholder="password" id="password" >
						<span class="help-block"><h5>En este campo tu pondras la Contraseña con el que el Usuario iniciara Secion</h5></span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4">
							<select class="form-control" id="Type">
								<option>Invitado</option>
								<option>Super Usuario</option>
								<option>Administrador</option>
							</select>
						<span class="help-block"><h5>En este campo tu pondras el tipo de permisos que tendra el Usuario</h5></span>
						</div>
						<div class="col-md-4">
							<input type="text" class="form-control" placeholder="Nombre" id="Name">
						<span class="help-block"><h5>En este campo tu pondras el Nombre del Usuario/<h5></span>
						</div>
						<div class="col-md-4">
							<input type="text" class="form-control" placeholder="Apellido" id="Lastname">
						<span class="help-block"><h5>En este campo tu pondras el Apellido del Usuario</h5></span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-4">
							<select class="form-control" id="generacion">
								<option>Generación</option>
							</select>
						<span class="help-block"><h5>En este campo tu pondras la Generacion del Usuario</h5></span>
						</div>
						<div class="col-md-4">
							<select class="form-control" id="carrera">
								<option>Carrera</option>
							</select>
						<span class="help-block"><h5>En este campo tu pondras la Carrera del Usuario</h5></span>
						</div>
						<div class="col-md-4">
							<input type="file" class="form-control" id="AvatarImg">
						<span class="help-block"><h5>En este campo tu elegiras la imagen del Usuario</h5></span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-3 col-md-offset-9">
							<button type="submit" class="btn btn-warning form-control">Guardar</button>
						</div>
					</div>
				</form>
					
					<div class="row">
						<div class="progress progress-striped active">
							<div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
						</div>
					</div>
			</div>
			

			<?php include_once($layoutsDirectory."/footerLayout.php"); ?>
		</div>


	</body>
</html>
				