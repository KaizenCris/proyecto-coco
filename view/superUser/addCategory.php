<?php
/***ESPECIFICAR $rootDirectory PARA CADA ARCHIVO DE CADA CARPETA PARA QUE FUNCIONE DE MANERA CORRECTA*******/
$rootDirectory = "../..";
$layoutsDirectory = $rootDirectory . "/layouts" ;
?>

<script type="text/javascript">
/*********************CONTROLADOR*********************************/
	<?php include_once($rootDirectory."/controller/categoryController/modifie.php");?>	
</script>
<!-- ESTA ZONA ES DEL EQUIPO VISTA -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title> CETI Proyectos
		</title>
		<link rel="shortcut icon" href="img/favicon.ico" />
	<!--Agrego css desde carpeta Layout-->
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/headerStyle.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/footerStyle.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<!--Agregen mas links Css y Javascript AQUI-->
		<script src="js/addProjectOnload.js"></script>
	</head>
	<body>
		<div id="content">
			<div class="container">
				<?php include_once($layoutsDirectory."/superUserLayout.php"); ?>

				<div class="jumbotron">
					<div class="row">
						<div class="progress progress-striped active">
							<div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="page-header text-center">
						 	<h1>Agregar <small>Categoria para Proyectos</small></h1>
						</div>
					</div>
					<div class="row">
						<div class="progress progress-striped active">
							<div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
							</div>
						</div>
					</div>
					<div class="row">
					<!-- en el formularioa cada campo se llama como en la base de datos solo falta el action=""
					-->
						<form class="form-horizontal" role="form" action="../../controller/categoryController/add.php" method="post">
							<div class="col-lg-12 form-group">
							  	<spam  class="col-lg-offset-2 col-lg-2 label label-warning text-center">Categoria</spam>
							    <div class="col-lg-6">
							    	<input type="text" id="Name" class="form-control text-center"  placeholder="una Categoria para Proyecto">
							    	<span class="help-block"><h5>En este campo tu pondras una TAG con la cual se podra hacer una busqueda de proyecto.</h5></span>
							    </div>
							</div>
							<div class="col-lg-12 form-group">
							    <div class="col-lg-offset-8 col-lg-2">
							      <button type="submit" class="btn btn-default">Levantar</button>
							    </div>
							</div>
						</form>
					</div>
					<div class="row">
						<div class="progress progress-striped active">
							<div class="progress-bar progress-bar-warning"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include_once($layoutsDirectory."/footerLayout.php"); ?>
	</body>
</html>