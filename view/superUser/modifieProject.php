<?php
/***ESPECIFICAR $rootDirectory PARA CADA ARCHIVO DE CADA CARPETA PARA QUE FUNCIONE DE MANERA CORRECTA*******/
$rootDirectory = "../..";
$layoutsDirectory = $rootDirectory . "/layouts" ;
?>

<script type="text/javascript">
/*********************CONTROLADOR*********************************/
	<?php include_once($rootDirectory."/controller/projectController/modifie.php");?>	
</script>
<!-- ESTA ZONA ES DEL EQUIPO VISTA -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title> CETI Proyectos</title>
		<link rel="shortcut icon" href="img/favicon.ico" />
	<!--Agrego css desde carpeta Layout-->
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/headerStyle.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/footerStyle.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<!--Agregen mas links Css y Javascript AQUI-->
		<script src="js/addProjectOnload.js"></script>
	</head>
	<body>

	
		<div class="container">
			
			<?php include_once($layoutsDirectory."/superUserLayout.php"); ?>
			<div class="jumbotron">
				<form class="form-horizontal" role="form">

					<div class="page-header">
					  <h2>Modificar Proyectos</h2>
					</div>
					<div class="form-group">
						<div class="col-lg-4">
							<select class="form-control">
								<?php 
										$Generations = $controllerData['generacion'];
										foreach ($Generations as $Generation) 
										{
											?>
												<option>
													<?php echo $Generation['nombreGen'] ?>//esto estara en español o en ingles? el modelo es en español?
												</option>
											<?php 		
										} 	
								 	?>
							</select>
						</div>
						<div class="col-lg-4">
							<select class="form-control">
								<?php 
										$Users = $controllerData['user'];
										foreach ($Users as $User) 
										{
											?>
												<option>
													<?php echo $User['name'] ?>//esto estara en español o en ingles? el modelo es en español?
												</option>
											<?php 		
										} 	
								 	?>
							</select>
						</div>
						<div class="col-lg-4">
							<select class="form-control">
								<?php 
										$Proyects = $controllerData['proyecto'];
										foreach ($Proyects as $Proyect) 
										{
											?>
												<option>
													<?php echo $Proyect['nombreProyecto'] ?>//esto estara en español o en ingles? el modelo es en español?
												</option>
											<?php 		
										} 	
								 	?>
							</select>
						</div>
					</div>
					<div class="page-header">
					  <h3>Datos Generales del Proyectos</h3>
					</div>
					<div class="form-group top">
						<div class="col-lg-6">
							<input type="text" class="form-control" id="Title" placeholder="Titulo">
						</div>
						<div class="col-lg-2">
							<label class="label-warning form-control text-center"><small>Imagen Portada</small></label>
						</div>
						<div class="col-lg-4">
							<input type="file" class="form-control" id="Front" >
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-2">
							<label class="label-warning form-control text-center"><small>Imagen Planeacion</small></label>
						</div>
						<div class="col-lg-4">
							<input type="file" class="form-control" id="PlaningImg" >
						</div>
						<div class="col-lg-2">
							<label class="label-warning form-control text-center"><small>Imagen Descripción</small></label>
						</div>
						<div class="col-lg-4">
							<input type="file" class="form-control" id="DescriptionImg" >
						</div>
					</div>
					<div class="page-header">
					  <h3>Descripción del Proyecto</h3>
					</div>
					<div class="form-group">
						<div class="col-lg-12">
							<textarea rows="6" type="text" id="Description" class="form-control"  placeholder="Descripcion de tu Proyecto"></textarea>
						</div>
					</div>
					<div class="page-header">
					  <h3>Planeación del Proyecto</h3>
					</div>
					<div class="form-group">
						<div class="col-lg-12">
							<textarea rows="6" type="text" id="Planing" class="form-control"  placeholder="Planeación del Proyecto"></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-2 col-lg-offset-10">
							<button type="submit" class="form-control btn btn-warning">Guardar</button>
						</div>
					</div>
				</form>
			</div>
			

			<?php include_once($layoutsDirectory."/footerLayout.php"); ?>
		</div>


	</body>
</html>