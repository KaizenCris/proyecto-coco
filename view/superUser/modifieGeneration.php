<?php
/***ESPECIFICAR $rootDirectory PARA CADA ARCHIVO DE CADA CARPETA PARA QUE FUNCIONE DE MANERA CORRECTA*******/
$rootDirectory = "../..";
$layoutsDirectory = $rootDirectory . "/layouts" ;
?>

<script type="text/javascript">
/*********************CONTROLADOR*********************************/
	<?php include_once($rootDirectory."/controller/projectController/modifie.php");?>	
</script>
<!-- ESTA ZONA ES DEL EQUIPO VISTA -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title> CETI Proyectos</title>
	<!--Agrego css desde carpeta Layout-->
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/headerStyle.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo $layoutsDirectory;?>/css/footerStyle.css" media="screen" />
	
	<!--Agregen mas links Css y Javascript AQUI-->
		<script src="js/indexOnload.js"></script>

		<meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    
	    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/estilo.css">
	</head>
	<body>
		
		<div class="container">
			
			<?php include_once($layoutsDirectory."/superUserLayout.php"); ?>
			<div class="jumbotron">

				<form class="form-horizontal" role="form" method="post">
					<div class="form-group">
						<div class=" col-lg-6">
							<div class="page-header">
								<h3>Generación Actual</h3>
							</div>						
							<select class="form-control" id="category">
								<option>Seleccionar Generación</option>	
							</select>
						</div>
						<div class=" col-lg-6">
							<div class="page-header">
								<h3>Nueva Generación</h3>
							</div>						
							<input type="text" class="form-control" id="newCategory">
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-3 col-lg-offset-9">
							<button class="btn btn-warning form-control">Guardar</button>
						</div>
					</div>

				</form>
			</div>

			<div id="content">
			
				<!-- 
					Ejemplo:
					Formulario para ingresar el nuevo
				-->
				<p>formulario agrega mi proyecto</p>
				
			</div>
			<?php include_once($layoutsDirectory."/footerLayout.php"); ?>

		</div>
	</body>
</html>